package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Like;
import org.springframework.stereotype.Repository;

/**
 * (Like)表数据库访问层
 *
 * @author thing7
 * @since 2024-05-09 10:02:50
 */
@Repository
public interface LikeMapper extends BaseMapper<Like> {

}

