package com.example.demo.controller;



import com.example.demo.entity.Like;
import com.example.demo.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * (Like)表控制层
 *
 * @author thing7
 * @since 2024-05-09 10:02:50
 */
@RestController
@RequestMapping("like")
public class LikeController {
    @Autowired
    private LikeService likeService;

    @PostMapping("/like")
    public String like(@RequestBody Like like){
       return likeService.saveLike(like);
    }
    @PostMapping("/unlike")
    public String unlike(@RequestBody Like like){
       return likeService.unlikeLike(like);
    }
    @GetMapping("/add")
    public void add(){
        likeService.transLikedFromRedis2DB();
    }

}

