package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Like;

/**
 * (Like)表服务接口
 *
 * @author thing7
 * @since 2024-05-09 10:02:51
 */
public interface LikeService extends IService<Like> {
    /**
     * 保存点赞记录
     *
     * @param
     * @return
     */
    String saveLike(Like like);
    String unlikeLike(Like like);
    /**
     * 将Redis里的点赞数据存入数据库中
     */
    void transLikedFromRedis2DB();



}


