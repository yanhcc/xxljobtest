package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.LikeMapper;
import com.example.demo.entity.Like;
import com.example.demo.service.LikeService;
import com.example.demo.service.RedisService;
import com.example.demo.utils.LikedStatusEnum;
import com.example.demo.utils.RedisKeyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Like)表服务实现类
 *
 * @author thing7
 * @since 2024-05-09 10:02:51
 */
@Service("likeService")
public class LikeServiceImpl extends ServiceImpl<LikeMapper, Like> implements LikeService {
    @Autowired
    private RedisService redisService;
    @Autowired
    private LikeMapper likeMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public String saveLike(Like like) {
        String key = RedisKeyUtils.getLikedKey(like.getUserId(), like.getPostId());
        redisTemplate.opsForHash().put(RedisKeyUtils.MAP_KEY_USER_LIKED, key, LikedStatusEnum.LIKE.getCode());
        return "点赞";
    }

    @Override
    public String unlikeLike(Like like) {
        String key = RedisKeyUtils.getLikedKey(like.getUserId(), like.getPostId());
        redisTemplate.opsForHash().put(RedisKeyUtils.MAP_KEY_USER_LIKED, key, LikedStatusEnum.UNLIKE.getCode());
        return "取消点赞";
    }

    @Override
    public void transLikedFromRedis2DB() {

        List<Like> list = redisService.getLikedDataFromRedis();
        for (Like like : list) {
            System.out.println(like.toString());
            LambdaQueryWrapper<Like> likeLambdaQueryWrapper = new LambdaQueryWrapper<>();
            likeLambdaQueryWrapper.eq(Like::getUserId, like.getUserId())
                    .eq(Like::getPostId, like.getPostId());
            Like demo = likeMapper.selectOne(likeLambdaQueryWrapper);
            System.out.println("ddemo"+demo.toString());
            if (demo == null){
                likeMapper.insert(like);
                //没有记录，直接存入
            }else{
                //有记录，需要更新
                demo.setStatus(like.getStatus());
                likeMapper.updateById(demo);
            }
        }
    }
}


