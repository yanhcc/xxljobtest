package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableName;


/**
 * (Like)表实体类
 *
 * @author thing7
 * @since 2024-05-09 10:02:46
 */
@TableName("userLike")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Like implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)

    private String likeId;
//点赞人id
    private String userId;
//事物id
    private String postId;
//点赞状态
    private Integer status;

    public Like(String userId, String postId, Integer status) {
        this.userId = userId;
        this.postId = postId;
        this.status = status;
    }
}

